import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages.component';
import { BlankComponent } from './blank/blank.component';
import { SearchComponent } from './search/search.component';

export const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), data: { breadcrumb: 'Dashboard' } },
      { path: 'maps', loadChildren: () => import('./maps/maps.module').then(m => m.MapsModule), data: { breadcrumb: 'Maps' } },
      { path: 'charts', loadChildren: () => import('./charting/charting.module').then(m => m.ChartingModule), data: { breadcrumb: 'Charts' } },
      { path: 'ui', loadChildren: () => import('./ui/ui.module').then(m => m.UiModule), data: { breadcrumb: 'UI' } },
      { path: 'tools', loadChildren: () => import('./tools/tools.module').then(m => m.ToolsModule), data: { breadcrumb: 'Tools' } },
      { path: 'mail', loadChildren: () => import('./mail/mail.module').then(m => m.MailModule), data: { breadcrumb: 'Mail' } },
      { path: 'calendar', loadChildren: () => import('./calendar/calendar.module').then(m => m.CalendarModule), data: { breadcrumb: 'Calendar' } },
      { path: 'form-elements', loadChildren: () => import('./form-elements/form-elements.module').then(m => m.FormElementsModule), data: { breadcrumb: 'Form Elements' } },
      { path: 'tables', loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule), data: { breadcrumb: 'Tables' } },
      { path: 'editors', loadChildren: () => import('./editors/editors.module').then(m => m.EditorsModule), data: { breadcrumb: 'Editors' } },
      { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule), data: { breadcrumb: 'Profile' } },
      { path: 'prueba', loadChildren: () => import('./prueba/prueba.module').then(m => m.PruebaModule), data: { breadcrumb: 'Prueba' } },
      // pantallas fase A
      { path: 'faseA/pantallaA1', loadChildren: () => import('./phases/phase-a/screen-a1/screenA1.module').then(m => m.ScreenA1Module), data: { breadcrumb: 'Pantalla A1' } },
      { path: 'faseA/pantallaA2', loadChildren: () => import('./phases/phase-a/screen-a2/screenA2.module').then(m => m.ScreenA2Module), data: { breadcrumb: 'Pantalla A2' } },
      { path: 'faseA/pantallaA3', loadChildren: () => import('./phases/phase-a/screen-a3/screenA3.module').then(m => m.ScreenA3Module), data: { breadcrumb: 'Pantalla A3' } },
      { path: 'faseA/pantallaA4', loadChildren: () => import('./phases/phase-a/screen-a4/screenA4.module').then(m => m.ScreenA4Module), data: { breadcrumb: 'Pantalla A4' } },
      { path: 'faseA/pantallaA5', loadChildren: () => import('./phases/phase-a/screen-a5/screenA5.module').then(m => m.ScreenA5Module), data: { breadcrumb: 'Pantalla A5' } },
      { path: 'faseA/pantallaA6', loadChildren: () => import('./phases/phase-a/screen-a6/screenA6.module').then(m => m.ScreenA6Module), data: { breadcrumb: 'Pantalla A6' } },
      // pantallas fase B
      { path: 'faseB/pantallaB1', loadChildren: () => import('./phases/phase-b/screen-b1/screenB1.module').then(m => m.ScreenB1Module), data: { breadcrumb: 'Pantalla B1' } },
      { path: 'faseB/pantallaB2', loadChildren: () => import('./phases/phase-b/screen-b2/screenB2.module').then(m => m.ScreenB2Module), data: { breadcrumb: 'Pantalla B2' } },
      { path: 'faseB/pantallaB3', loadChildren: () => import('./phases/phase-b/screen-b3/screenB3.module').then(m => m.ScreenB3Module), data: { breadcrumb: 'Pantalla B3' } },
      { path: 'faseB/pantallaB4', loadChildren: () => import('./phases/phase-b/screen-b4/screenB4.module').then(m => m.ScreenB4Module), data: { breadcrumb: 'Pantalla B4' } },
      { path: 'faseB/pantallaB5', loadChildren: () => import('./phases/phase-b/screen-b5/screenB5.module').then(m => m.ScreenB5Module), data: { breadcrumb: 'Pantalla B5' } },
      { path: 'faseB/pantallaB6', loadChildren: () => import('./phases/phase-b/screen-b6/screenB6.module').then(m => m.ScreenB6Module), data: { breadcrumb: 'Pantalla B6' } },
      { path: 'faseB/pantallaB7', loadChildren: () => import('./phases/phase-b/screen-b7/screenB7.module').then(m => m.ScreenB7Module), data: { breadcrumb: 'Pantalla B7' } },
      { path: 'faseB/pantallaB8', loadChildren: () => import('./phases/phase-b/screen-b8/screenB8.module').then(m => m.ScreenB8Module), data: { breadcrumb: 'Pantalla B8' } },
      // pantallas fase C
      { path: 'faseC/pantallaC1', loadChildren: () => import('./phases/phase-c/screen-c1/screenC1.module').then(m => m.ScreenC1Module), data: { breadcrumb: 'Pantalla C1' } },
      { path: 'faseC/pantallaC2', loadChildren: () => import('./phases/phase-c/screen-c2/screenC2.module').then(m => m.ScreenC2Module), data: { breadcrumb: 'Pantalla C2' } },
      { path: 'faseC/pantallaC3', loadChildren: () => import('./phases/phase-c/screen-c3/screenC3.module').then(m => m.ScreenC3Module), data: { breadcrumb: 'Pantalla C3' } },
      { path: 'faseC/pantallaC4', loadChildren: () => import('./phases/phase-c/screen-c4/screenC4.module').then(m => m.ScreenC4Module), data: { breadcrumb: 'Pantalla C4' } },
      { path: 'faseC/pantallaC5', loadChildren: () => import('./phases/phase-c/screen-c5/screenC5.module').then(m => m.ScreenC5Module), data: { breadcrumb: 'Pantalla C5' } },
      { path: 'faseC/pantallaC6', loadChildren: () => import('./phases/phase-c/screen-c6/screenC6.module').then(m => m.ScreenC6Module), data: { breadcrumb: 'Pantalla C6' } },
      { path: 'faseC/pantallaC7', loadChildren: () => import('./phases/phase-c/screen-c7/screenC7.module').then(m => m.ScreenC7Module), data: { breadcrumb: 'Pantalla C7' } },
      { path: 'faseC/pantallaC8', loadChildren: () => import('./phases/phase-c/screen-c8/screenC8.module').then(m => m.ScreenC8Module), data: { breadcrumb: 'Pantalla C8' } },
      { path: 'search', component: SearchComponent, data: { breadcrumb: 'Search' } },
      { path: 'blank', component: BlankComponent, data: { breadcrumb: 'Blank page' } }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule { }
