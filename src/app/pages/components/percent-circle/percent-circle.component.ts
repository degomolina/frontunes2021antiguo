import { Component, ViewEncapsulation, Input, ElementRef, ViewChild, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { AppConfig } from "../../../app.config";
import * as Chart from 'chart.js';
import { duration } from 'moment';

@Component({
  selector: '.az-percent-circle',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './percent-circle.component.html',
  styleUrls: ['./percent-circle.component.scss'],
})
export class PercentCircleComponent implements AfterViewInit, OnChanges {

  @Input() public label = '';
  @Input() public percentValue = 0;
  @Input() public canvasId = '';

  countEventsData: Chart.ChartDataSets[] = [
    { data: [this.percentValue, 100 - this.percentValue], label: "Number of Events", fill: false }
  ];

  @ViewChild('canvas') localCanvasNode: ElementRef;

  public config: any;
  public configFn: any;

  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  // circulo con porcentaje
  canvas: any;
  ctx: any;
  chart: any;

  public red_min_hex = '45';
  public red_min_dec = parseInt(this.red_min_hex, 16);
  public red_max_hex = 'cc';
  public red_max_dec = parseInt(this.red_max_hex, 16);
  public green_min_hex = '35';
  public green_min_dec = parseInt(this.green_min_hex, 16);
  public green_max_hex = 'ac';
  public green_max_dec = parseInt(this.green_max_hex, 16);
  public blue_min_hex = '20';
  public blue_min_dec = parseInt(this.blue_min_hex, 16);
  public blue_max_hex = '78';
  public blue_max_dec = parseInt(this.blue_max_hex, 16);

  public pi = Math.PI;

  // corre solo una vez durante el ciclo de vida del componente
  ngAfterViewInit() {
    this.canvas = this.localCanvasNode.nativeElement;
    this.ctx = this.canvas.getContext('2d');

    let percentVal = this.percentValue;
    let datasets = [{
      "data": [percentVal, 100 - percentVal],
      "backgroundColor": ["#e0e0e0", "#e0e0e0"]
    }];

    var chartData = {
      type: 'doughnut',
      data: { datasets: datasets },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 90,
        segmentShowStroke: false,
        events: [],
        elements: {
          arc: {
            roundCorners: 0,
            borderWidth: 0
          },
          center: {
            maxText: "100%",
            text: `${datasets[0].data[0]}%`,
            fontColor: "#646464",
            fontFamily: "Roboto",
            fontStyle: "normal",
            minFontSize: 20,
            maxFontSize: 90
          },
          centerSub: {
            // text: `${datasets[0].data[1]} is the remainder.`,
            text: this.label,
            fontColor: "#a6a6a6",
            minFontSize: 10,
            maxFontSize: 20,

          },
          responsiveAnimationDuration: 0 // animation duration after a resize
        },
        hover: {
          animationDuration: 0 // duration of animations when hovering an item
        },
        animation: {
          duration: 1,
          onProgress: animation => {
            animation.easing = 'linear';
            this.animateArc(animation.chart);

          },
        }
      }
    }

    this.chart = new Chart(this.ctx, {
      ...chartData,
      plugins: [{
        beforeDraw: chart => {
          this.drawArc(chart, null, '#e0e0e0');
        },
        afterUpdate: chart => {
          this.addCenterTextAfterUpdate(chart);
          this.roundCornersAfterUpdate(chart);
        },
        afterDraw: chart => {
          this.addCenterTextAfterDraw(chart);
          this.roundCornersAfterDraw(chart);
        },
        resize: () => new Chart(this.ctx, {
          ...chartData,
          plugins: [{
            beforeDraw: chart => {
              this.drawArc(chart, null, '#e0e0e0');
            },
            afterUpdate: chart => {
              this.addCenterTextAfterUpdate(chart);
              this.roundCornersAfterUpdate(chart);
            },
            afterDraw: chart => {
              this.addCenterTextAfterDraw(chart);
              this.roundCornersAfterDraw(chart);
            },
          }]
        })
      }]
    });
  }

  // no sé si será recomendado actualizar con este método, las propiedades
  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    if (!changes.percentValue.firstChange) {
      let newCountEventsData: Chart.ChartDataSets[] = [
        {
          data: [changes.percentValue.currentValue, 100 - changes.percentValue.currentValue],
          backgroundColor: ["#e0e0e0", "#e0e0e0"],
        }
      ];

      this.countEventsData = newCountEventsData;
      this.updateConfigByMutating(this.chart, newCountEventsData);
    }
  }

  updateConfigByMutating(chart, datasets) {
    chart.data.datasets = datasets;
    chart.options.elements.center.text = `${datasets[0].data[0]}%`;
    chart.update();
  }

  chartClicked(e: any): void {
    //console.log(e);
  }

  chartHovered(e: any): void {
    //console.log(e);
  }

  animateArc = chart => {
    const pi = Math.PI;
    let arc = chart.getDatasetMeta(0).data[0];
    let angle = arc._view.endAngle + pi / 2;
    let angle_inverse = 2 * pi - angle;
    let blue = Math.round(
      (angle / (2 * pi)) * this.blue_max_dec + (angle_inverse / (2 * pi)) * this.blue_min_dec
    ).toString(16);
    if (arc._view.endAngle < pi / 2) {
      let green = Math.round(
        (angle / pi) * this.green_max_dec + ((pi - angle) / pi) * this.green_min_dec
      ).toString(16);
      if (green.length < 2) green = '0' + green;
      let color = `#${this.red_max_hex}${green}${blue}`;
      arc.round.backgroundColor = color;
      this.drawArc(chart, arc, color);
    } else {
      let red = Math.round(
        ((2 * pi - angle) / pi) * this.red_max_dec + ((angle - pi) / pi) * this.red_min_dec
      ).toString(16);
      if (red.length < 2) red = '0' + red;
      if (red === '45') red = '50';
      if (blue === '78') blue = '74';
      let color = `#${red}${this.green_max_hex}${blue}`;
      arc.round.backgroundColor = color;
      this.drawArc(chart, arc, color);
    }
  }

  drawArc = (chart, arc, color) => {
    let x = (chart.chartArea.left + chart.chartArea.right) / 2;
    let y = (chart.chartArea.top + chart.chartArea.bottom) / 2;
    chart.ctx.fillStyle = color;
    chart.ctx.strokeStyle = color;
    chart.ctx.beginPath();
    if (arc != null) {
      chart.ctx.arc(x, y, chart.outerRadius, arc._view.startAngle, arc._view.endAngle);
      chart.ctx.arc(x, y, chart.innerRadius, arc._view.endAngle, arc._view.startAngle, true);
    } else {
      chart.ctx.arc(x, y, chart.outerRadius, 0, 2 * this.pi);
      chart.ctx.arc(x, y, chart.innerRadius, 0, 2 * this.pi, true);
    }
    chart.ctx.fill();
  }

  addCenterTextAfterUpdate = chart => {
    if (
      chart.config.options.elements.center &&
      chart.config.options.elements.centerSub &&
      chart.ctx
    ) {
      const centerConfig = chart.config.options.elements.center;
      const centerConfigSub = chart.config.options.elements.centerSub;
      const globalConfig = Chart.defaults.global;
      let fontStyle = centerConfig.fontStyle;
      let fontFamily = Chart.helpers.getValueOrDefault(centerConfig.fontFamily, 'Roboto');
      let fontSize = Chart.helpers.getValueOrDefault(centerConfig.minFontSize, 20);
      let maxFontSize = Chart.helpers.getValueOrDefault(centerConfig.maxFontSize, 90);
      let maxText = Chart.helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);
      do {
        chart.ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
        let textWidth = chart.ctx.measureText(maxText).width;
        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize) fontSize += 1;
        else {
          fontSize -= 1;
          break;
        }
      } while (true);
      chart.center = {
        font: Chart.helpers.fontString(fontSize, fontStyle, fontFamily),
        fillStyle: Chart.helpers.getValueOrDefault(
          centerConfig.fontColor,
          globalConfig.defaultFontColor
        ),
      };
      fontSize = Chart.helpers.getValueOrDefault(centerConfigSub.minFontSize, 10);
      maxFontSize = Chart.helpers.getValueOrDefault(centerConfigSub.maxFontSize, 25);
      maxText = centerConfigSub.text;
      do {
        chart.ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
        let textWidth = chart.ctx.measureText(maxText).width;
        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize) fontSize += 1;
        else {
          fontSize -= 1;
          break;
        }
      } while (true);
      chart.centerSub = {
        font: Chart.helpers.fontString(fontSize, fontStyle, fontFamily),
        fillStyle: Chart.helpers.getValueOrDefault(
          centerConfigSub.fontColor,
          globalConfig.defaultFontColor
        ),
      };
    }
  }

  roundCornersAfterUpdate = chart => {
    if (chart.config.options.elements.arc.roundCorners !== undefined) {
      let arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundCorners];
      arc.round = {
        x: (chart.chartArea.left + chart.chartArea.right) / 2,
        y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
        radius: (chart.outerRadius + chart.innerRadius) / 2,
        thickness: (chart.outerRadius - chart.innerRadius) / 2,
        backgroundColor: arc._model.backgroundColor,
      };
    }
  };

  addCenterTextAfterDraw = chart => {
    if (chart.center && chart.centerSub) {
      chart.ctx.textAlign = 'center';
      chart.ctx.textBaseline = 'middle';
      const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
      const centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
      const lowerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 + 65;
      const centerConfig = chart.config.options.elements.center;
      chart.ctx.font = chart.center.font;
      chart.ctx.fillStyle = chart.center.fillStyle;
      chart.ctx.fillText(centerConfig.text, centerX, centerY);
      const centerSubConfig = chart.config.options.elements.centerSub;
      chart.ctx.font = chart.centerSub.font;
      chart.ctx.fillStyle = chart.centerSub.fillStyle;
      chart.ctx.fillText(centerSubConfig.text, centerX, lowerY);
    }
  };

  roundCornersAfterDraw = chart => {
    if (chart.config.options.elements.arc.roundCorners !== undefined) {
      var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundCorners];
      var startAngle = this.pi / 2 - arc._view.startAngle;
      var endAngle = this.pi / 2 - arc._view.endAngle;
      chart.ctx.save();
      chart.ctx.translate(arc.round.x, arc.round.y);
      chart.ctx.fillStyle = arc.round.backgroundColor;
      chart.ctx.beginPath();
      chart.ctx.arc(
        arc.round.radius * Math.sin(startAngle),
        arc.round.radius * Math.cos(startAngle),
        arc.round.thickness,
        0,
        2 * this.pi,
      );
      chart.ctx.arc(
        arc.round.radius * Math.sin(endAngle),
        arc.round.radius * Math.cos(endAngle),
        arc.round.thickness,
        0,
        2 * this.pi,
      );
      chart.ctx.fill();
      chart.ctx.restore();
    }
  };


}
