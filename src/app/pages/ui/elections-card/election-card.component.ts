import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'az-election-card',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './election-card.component.html'
})
export class ElectionCardComponent {
  @Input() public ecTitle = '';
}
