import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../../app.config";
// import { select, scaleBand, scaleLinear } from 'd3';
import * as Chart from 'chart.js';

@Component({
  selector: '.az-voters',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './voters.component.html',
  styleUrls: ['./voters.component.scss'],
})
export class VotersComponent {
  public config: any;
  public configFn: any;

  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  // circulo con porcentaje
  canvas: any;
  ctx: any;

  public red_min_hex = '45';
  public red_min_dec = parseInt(this.red_min_hex, 16);
  public red_max_hex = 'cc';
  public red_max_dec = parseInt(this.red_max_hex, 16);
  public green_min_hex = '35';
  public green_min_dec = parseInt(this.green_min_hex, 16);
  public green_max_hex = 'ac';
  public green_max_dec = parseInt(this.green_max_hex, 16);
  public blue_min_hex = '20';
  public blue_min_dec = parseInt(this.blue_min_hex, 16);
  public blue_max_hex = '78';
  public blue_max_dec = parseInt(this.blue_max_hex, 16);

  public pi = Math.PI;

  ngOnInit() {

    // const DUMMY_DATA = [
    //   {id: 'd1', value: 10, region: 'Riobamba'},
    //   {id: 'd2', value: 13, region: 'Carchi'},
    //   {id: 'd3', value: 120, region: 'Cuenca'},
    //   {id: 'd4', value: 110, region: 'Machala'},
    //   {id: 'd5', value: 140, region: 'Guayaquil'},
    // ];

    // const xScale = scaleBand()
    //   .domain(DUMMY_DATA.map((datapoint) => datapoint.region))
    //   .rangeRound([0, 250])
    //   .padding(0.1);
    // const yScale = scaleLinear().domain([0, 150]).range([200, 0]);

    // const container = select('#test')
    //   .classed('container', true);

    // const bars = container
    //   .selectAll('.bar')
    //   .data(DUMMY_DATA)
    //   .enter()
    //   .append('rect')
    //   .classed('bar', true)
    //   .attr('width', xScale.bandwidth())
    //   .attr('height', data => 200 - yScale(data.value))
    //   .attr('x', data => xScale(data.region))
    //   .attr('y', data => yScale(data.value));

    // setTimeout(() => {
    //   bars.data(DUMMY_DATA.slice(0, 2)).exit().remove();
    // }, 1000);

    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');

    let datasets = [{
      "data": [84, 16],
      "backgroundColor": ["#e0e0e0", "#e0e0e0"]
    }];

    var chartData = {
      type: 'doughnut',
      data: { datasets: datasets },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 90,
        segmentShowStroke: false,
        events: [],
        elements: {
          arc: {
            roundCorners: 0,
            borderWidth: 0
          },
          center: {
            maxText: "100%",
            text: `${datasets[0].data[0]}%`,
            fontColor: "#646464",
            fontFamily: "Roboto",
            fontStyle: "normal",
            minFontSize: 20,
            maxFontSize: 90
          },
          centerSub: {
            // text: `${datasets[0].data[1]} is the remainder.`,
            text: `Sufragantes`,
            fontColor: "#a6a6a6",
            minFontSize: 10,
            maxFontSize: 25,

          }
        },
        animation: {
          onProgress: animation => {
            animation.easing = 'linear';
            this.animateArc(animation.chart)
          }
        }
      }
    }

    let chart = new Chart(this.ctx, {
      ...chartData,
      plugins: [{
        beforeDraw: chart => {
          this.drawArc(chart, null, '#e0e0e0');
        },
        afterUpdate: chart => {
          this.addCenterTextAfterUpdate(chart);
          this.roundCornersAfterUpdate(chart);
        },
        afterDraw: chart => {
          this.addCenterTextAfterDraw(chart);
          this.roundCornersAfterDraw(chart);
        },
        resize: () => new Chart(this.ctx, {
          ...chartData,
          plugins: [{
            beforeDraw: chart => {
              this.drawArc(chart, null, '#e0e0e0');
            },
            afterUpdate: chart => {
              this.addCenterTextAfterUpdate(chart);
              this.roundCornersAfterUpdate(chart);
            },
            afterDraw: chart => {
              this.addCenterTextAfterDraw(chart);
              this.roundCornersAfterDraw(chart);
            },
          }]
        })
      }]
    });
  }

  public chartClicked(e: any): void {
    //console.log(e);
  }

  public chartHovered(e: any): void {
    //console.log(e);
  }

  public animateArc = chart => {
    const pi = Math.PI;
    let arc = chart.getDatasetMeta(0).data[0];
    let angle = arc._view.endAngle + pi / 2;
    let angle_inverse = 2 * pi - angle;
    let blue = Math.round(
      (angle / (2 * pi)) * this.blue_max_dec + (angle_inverse / (2 * pi)) * this.blue_min_dec
    ).toString(16);
    if (arc._view.endAngle < pi / 2) {
      let green = Math.round(
        (angle / pi) * this.green_max_dec + ((pi - angle) / pi) * this.green_min_dec
      ).toString(16);
      if (green.length < 2) green = '0' + green;
      let color = `#${this.red_max_hex}${green}${blue}`;
      arc.round.backgroundColor = color;
      this.drawArc(chart, arc, color);
    } else {
      let red = Math.round(
        ((2 * pi - angle) / pi) * this.red_max_dec + ((angle - pi) / pi) * this.red_min_dec
      ).toString(16);
      if (red.length < 2) red = '0' + red;
      if (red === '45') red = '50';
      if (blue === '78') blue = '74';
      let color = `#${red}${this.green_max_hex}${blue}`;
      arc.round.backgroundColor = color;
      this.drawArc(chart, arc, color);
    }
  }

  public drawArc = (chart, arc, color) => {
    let x = (chart.chartArea.left + chart.chartArea.right) / 2;
    let y = (chart.chartArea.top + chart.chartArea.bottom) / 2;
    chart.ctx.fillStyle = color;
    chart.ctx.strokeStyle = color;
    chart.ctx.beginPath();
    if (arc != null) {
      chart.ctx.arc(x, y, chart.outerRadius, arc._view.startAngle, arc._view.endAngle);
      chart.ctx.arc(x, y, chart.innerRadius, arc._view.endAngle, arc._view.startAngle, true);
    } else {
      chart.ctx.arc(x, y, chart.outerRadius, 0, 2 * this.pi);
      chart.ctx.arc(x, y, chart.innerRadius, 0, 2 * this.pi, true);
    }
    chart.ctx.fill();
  }

  addCenterTextAfterUpdate = chart => {
    if (
      chart.config.options.elements.center &&
      chart.config.options.elements.centerSub &&
      chart.ctx
    ) {
      const centerConfig = chart.config.options.elements.center;
      const centerConfigSub = chart.config.options.elements.centerSub;
      const globalConfig = Chart.defaults.global;
      let fontStyle = centerConfig.fontStyle;
      let fontFamily = Chart.helpers.getValueOrDefault(centerConfig.fontFamily, 'Roboto');
      let fontSize = Chart.helpers.getValueOrDefault(centerConfig.minFontSize, 20);
      let maxFontSize = Chart.helpers.getValueOrDefault(centerConfig.maxFontSize, 90);
      let maxText = Chart.helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);
      do {
        chart.ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
        let textWidth = chart.ctx.measureText(maxText).width;
        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize) fontSize += 1;
        else {
          fontSize -= 1;
          break;
        }
      } while (true);
      chart.center = {
        font: Chart.helpers.fontString(fontSize, fontStyle, fontFamily),
        fillStyle: Chart.helpers.getValueOrDefault(
          centerConfig.fontColor,
          globalConfig.defaultFontColor
        ),
      };
      fontSize = Chart.helpers.getValueOrDefault(centerConfigSub.minFontSize, 10);
      maxFontSize = Chart.helpers.getValueOrDefault(centerConfigSub.maxFontSize, 25);
      maxText = centerConfigSub.text;
      do {
        chart.ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
        let textWidth = chart.ctx.measureText(maxText).width;
        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize) fontSize += 1;
        else {
          fontSize -= 1;
          break;
        }
      } while (true);
      chart.centerSub = {
        font: Chart.helpers.fontString(fontSize, fontStyle, fontFamily),
        fillStyle: Chart.helpers.getValueOrDefault(
          centerConfigSub.fontColor,
          globalConfig.defaultFontColor
        ),
      };
    }
  }

  roundCornersAfterUpdate = chart => {
    if (chart.config.options.elements.arc.roundCorners !== undefined) {
      let arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundCorners];
      arc.round = {
        x: (chart.chartArea.left + chart.chartArea.right) / 2,
        y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
        radius: (chart.outerRadius + chart.innerRadius) / 2,
        thickness: (chart.outerRadius - chart.innerRadius) / 2,
        backgroundColor: arc._model.backgroundColor,
      };
    }
  };

  addCenterTextAfterDraw = chart => {
    if (chart.center && chart.centerSub) {
      chart.ctx.textAlign = 'center';
      chart.ctx.textBaseline = 'middle';
      const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
      const centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
      const lowerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 + 65;
      const centerConfig = chart.config.options.elements.center;
      chart.ctx.font = chart.center.font;
      chart.ctx.fillStyle = chart.center.fillStyle;
      chart.ctx.fillText(centerConfig.text, centerX, centerY);
      const centerSubConfig = chart.config.options.elements.centerSub;
      chart.ctx.font = chart.centerSub.font;
      chart.ctx.fillStyle = chart.centerSub.fillStyle;
      chart.ctx.fillText(centerSubConfig.text, centerX, lowerY);
    }
  };

  roundCornersAfterDraw = chart => {
    if (chart.config.options.elements.arc.roundCorners !== undefined) {
      var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundCorners];
      var startAngle = this.pi / 2 - arc._view.startAngle;
      var endAngle = this.pi / 2 - arc._view.endAngle;
      chart.ctx.save();
      chart.ctx.translate(arc.round.x, arc.round.y);
      chart.ctx.fillStyle = arc.round.backgroundColor;
      chart.ctx.beginPath();
      chart.ctx.arc(
        arc.round.radius * Math.sin(startAngle),
        arc.round.radius * Math.cos(startAngle),
        arc.round.thickness,
        0,
        2 * this.pi,
      );
      chart.ctx.arc(
        arc.round.radius * Math.sin(endAngle),
        arc.round.radius * Math.cos(endAngle),
        arc.round.thickness,
        0,
        2 * this.pi,
      );
      chart.ctx.fill();
      chart.ctx.restore();
    }
  };


}
