import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import 'chart.js/dist/Chart.js';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenA1Component } from './screenA1.component';
import { VotersComponent } from './voters/voters.component';
import { ElectionCardComponent } from '../../../ui/elections-card/election-card.component';
import { PercentCircleComponent } from '../../../components/percent-circle/percent-circle.component';

export const routes = [
  { path: '', component: ScreenA1Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenA1Component,
    VotersComponent,
    PercentCircleComponent,
    ElectionCardComponent,
  ],
})

export class ScreenA1Module { }
