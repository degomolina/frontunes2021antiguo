import { Component, ViewEncapsulation, OnDestroy } from '@angular/core';
import { AppConfig } from '../../../../app.config';

import { ScreenA1Service } from './screen-a1.service';


@Component({
  selector: 'az-screen-a1',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenA1.component.html'
})
export class ScreenA1Component implements OnDestroy {
  public config: any;
  public configFn: any;

  public lineChartType: string = 'line';
  public lineChartLegend: boolean = true;
  public lineChartLabels: string[];
  public lineChartData: Array<any>;
  public lineChartColors: any[];
  public lineChartOptions: any;

  // datos de prueba circulos de porcentajes
  public nDelegadosConectados = 50.00;
  public nDelegadosActivos = 50.00;
  public nConteoRapido = 50.00;
  public nVotantes = 0;

  public setIntervalReference1 = null;
  public setIntervalReference2 = null;
  public setIntervalReference3 = null;
  public setIntervalReference4 = null;


  constructor(
    private _appConfig: AppConfig,
    private screenA1Service: ScreenA1Service,
  ) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnInit() {

    this.setIntervalReference1 = setInterval(() => {
      let aumento = Math.floor((Math.random() * 10) + 2);
      let coin = this.randomIntFromInterval(0, 1);
      if (this.nDelegadosConectados < 0) {
        this.nDelegadosConectados = 0;
      } else if (this.nDelegadosConectados > 100) {
        this.nDelegadosConectados = 100;
      } else {
        if (coin === 0) {
          this.nDelegadosConectados += aumento;
        } else {
          this.nDelegadosConectados -= aumento;
        }
      }
    }, 1500);

    this.setIntervalReference2 = setInterval(() => {
      let aumento = Math.floor((Math.random() * 10) + 1);
      let coin = this.randomIntFromInterval(0, 1);
      if (this.nDelegadosActivos < 0) {
        this.nDelegadosActivos = 0;
      } else if (this.nDelegadosActivos > 100) {
        this.nDelegadosActivos = 100;
      } else {
        if (coin === 0) {
          this.nDelegadosActivos += aumento;
        } else {
          this.nDelegadosActivos -= aumento;
        }
      }
    }, 2000);

    this.setIntervalReference3 = setInterval(() => {
      let aumento = Math.floor((Math.random() * 10) + 3);
      let coin = this.randomIntFromInterval(0, 1);
      if (this.nConteoRapido < 0) {
        this.nConteoRapido = 0;
      } else if (this.nConteoRapido > 100) {
        this.nConteoRapido = 100;
      } else {
        if (coin === 0) {
          this.nConteoRapido += aumento;
        } else {
          this.nConteoRapido -= aumento;
        }
      }
    }, 1200);


    this.setIntervalReference4 = setInterval(() => {
      let pData = this.screenA1Service.getParticipationData(this.nVotantes);
      this.nVotantes = pData.newValue;
      this.lineChartLabels.push(pData.label);
      this.lineChartData[0].data.push(pData.newValue);
    }, 1000);


    //--- Line Chart ---
    this.lineChartLabels = [''];
    this.lineChartData = [
      {
        data: [this.nVotantes],
        label: 'Número de Votantes',
        fill: false,
      },
    ];
    this.lineChartColors = [
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.success, 0.5),
        borderColor: this.config.colors.success,
        pointBorderColor: this.config.colors.default,
        pointHoverBorderColor: this.config.colors.success,
        pointHoverBackgroundColor: this.config.colors.default,
        hoverBackgroundColor: this.config.colors.success
      },
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.warning, 0.5),
        borderColor: this.config.colors.warning,
        pointBorderColor: this.config.colors.default,
        pointHoverBorderColor: this.config.colors.warning,
        pointHoverBackgroundColor: this.config.colors.default,
        hoverBackgroundColor: this.config.colors.warning
      },
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.primary, 0.5),
        borderColor: this.config.colors.primary,
        pointBorderColor: this.config.colors.default,
        pointHoverBorderColor: this.config.colors.primary,
        pointHoverBackgroundColor: this.config.colors.default,
        hoverBackgroundColor: this.config.colors.primary
      }
    ];
    this.lineChartOptions = {
      scales: {
        yAxes: [{
          ticks: {
            fontColor: this.configFn.rgba(this.config.colors.gray, 0.7),
            beginAtZero: true
          },
          gridLines: {
            display: true,
            zeroLineColor: this.configFn.rgba(this.config.colors.gray, 0.5),
            zeroLineWidth: 1,
            color: this.configFn.rgba(this.config.colors.gray, 0.1)
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: this.configFn.rgba(this.config.colors.gray, 0.7)
          },
          gridLines: {
            display: true,
            zeroLineColor: this.configFn.rgba(this.config.colors.gray, 0.5),
            zeroLineWidth: 1,
            color: this.configFn.rgba(this.config.colors.gray, 0.1)
          }
        }]
      },
      legend: {
        labels: {
          fontColor: this.configFn.rgba(this.config.colors.gray, 0.9),
        }
      },
      tooltips: {
        enabled: true,
        backgroundColor: this.configFn.rgba(this.config.colors.main, 0.7)
      },
      animation: {
        duration: 0
      },
    }
  }

  public chartClicked(e: any): void {
    //console.log(e);
  }

  public chartHovered(e: any): void {
    //console.log(e);
  }

  ngOnDestroy() {
    clearInterval(this.setIntervalReference1);
    clearInterval(this.setIntervalReference2);
    clearInterval(this.setIntervalReference3);
    clearInterval(this.setIntervalReference4);
  }
}
