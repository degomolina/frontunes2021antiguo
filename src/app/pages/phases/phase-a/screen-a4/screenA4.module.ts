import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenA4Component } from './screenA4.component';
import { MapaComponent } from './mapa/datamap.component';

export const routes = [
  { path: '', component: ScreenA4Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenA4Component,
    MapaComponent,
  ]
})

export class ScreenA4Module { }
