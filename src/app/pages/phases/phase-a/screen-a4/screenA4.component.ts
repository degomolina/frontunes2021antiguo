import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../app.config";


@Component({
  selector: 'az-screen-a4',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenA4.component.html'
})
export class ScreenA4Component  { 
    public config:any;
    public configFn:any;

    constructor(private _appConfig:AppConfig){
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;       
    } 

    ngOnInit() { 
    }

    public chartClicked(e:any):void {
    //console.log(e);
    }

    public chartHovered(e:any):void {
    //console.log(e);
    }



}
