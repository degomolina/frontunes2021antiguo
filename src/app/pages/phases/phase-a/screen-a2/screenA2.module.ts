import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import 'chart.js/dist/Chart.js';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenA2Component } from './screenA2.component';
import { MapaIncidenciasComponent } from './mapa-incidencias/datamap.component';

export const routes = [
  { path: '', component: ScreenA2Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenA2Component,
    MapaIncidenciasComponent,
  ]
})

export class ScreenA2Module { }
