import { Component, ViewEncapsulation } from '@angular/core';
import {
  geoMercator,
  geoPath,
  json,
  select,
  geoEquirectangular,
  zoom,
  range,
  D3ZoomEvent
} from 'd3';
import { AppConfig } from "../../../../../app.config";
import { DataMapService } from './datamap.service';
import { feature } from 'topojson';

@Component({
  selector: 'az-mapa-incidencias',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './datamap.component.html',
  styleUrls: ['./datamap.component.scss'],
  providers: [DataMapService]
})
export class MapaIncidenciasComponent {

  public config: any;
  public configFn: any;
  public data: any;
  public bubbles: any;
  public selectedRegionName: any;
  

  constructor(private _dataMapService: DataMapService, private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
    this.data = _dataMapService.getData();
    this.bubbles = _dataMapService.getBubbles();
  }

  public ngAfterViewInit(): void {

    let width = 960,
      height = 500;

    const projection = geoEquirectangular()
      .scale(3200)
      .center([-84, -1.5])
      .translate([width / 3, height / 2]);

    const pathGenerator = geoPath().projection(projection);

    let rootDiv = select('#electionMap');

    let svg = rootDiv
      .append('svg:svg')
      .attr('viewBox', '0 0 ' + width + ' ' + height)
      .attr('perserveAspectRatio', 'xMinYMid');

    let tooltip = select("#tooltipContainer")
      .append("div")
      .attr("class", "");
    tooltip.html(" ");

    const g = svg.append("g");

    function selected() {
      select('.selected').classed('selected', false);
      select(this).classed('selected', true);
    }

    // let zooma = zoom()
    //   .scaleExtent([1, 8])
    //   .on("zoom", zoomed);

    // let zoomed = () => {
    //   g.style("stroke-width", 1.5 / d3.event.transform.k + "px");
    //   // g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")"); // not in d3 v4
    //   g.attr("transform", d3.event.transform); // updated for d3 v4
    // }

    // json<any>('/assets/data/ecuador_parroquias.json')
    json<any>('/assets/data/ecuador.geojson')
      .then(data => {
        const geojson = feature(data, data.features);
        // const geojson = feature(data, data.objects.ecuador_parroquias);
        // console.log(data.features);

        // compute new area for each state
        data.features = data.features.map(function (d, i) {

          if (d.properties.codigo === 20) {
            // console.log('d', d.geometry.coordinates);
            let area = pathGenerator.area(d.geometry);
            d.geometry.coordinates = d.geometry.coordinates.map(coord => {
              // console.log(coord);
              return coord;
            });
            // console.log(area);
            return d;
          }
          return d;

        });

        svg.selectAll('path').data(data.features)
          .enter().append('path')
          .attr('id', (d: any) => 'prov_' + d.properties.id_prov)
          .attr('transform', (d: any) => {
            if (d.properties.id_prov === 20) {
              return 'translate(400, 0)'
            }
            return '';
          })
          .attr('class', 'provincia')
          .attr('stroke', '#aaa')
          .attr('stroke-width', '0.5')
          .on('click', selected)
          .on("mousemove", function (_, d2: any) {
            let label = d2.properties.DPA_DESPAR;
            tooltip.classed('hidden', false)
              .html(label)
          })

          .attr('d', pathGenerator);
      });

    let galap = svg.select('path#prov_20')

    console.log(galap);

    svg.select('path#prov_20').style('fill', (d: any) => { console.log(); return d })
  }



}
