import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../app.config";


@Component({
  selector: 'az-screen-a3',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenA3.component.html'
})
export class ScreenA3Component {
  public config: any;
  public configFn: any;

  public barChartType: string = 'bar';
  public barChartTypeH: string = 'horizontalBar';
  public barChartLegend: boolean = true;
  public barChartLabels: string[];
  public barChartData: Array<any>;
  public barChartColors: any[];
  public barChartOptions: any;

  public doughnutChartType: string = 'doughnut';
  public pieChartType: string = 'pie';
  public doughnutChartLegend: boolean = true;
  public doughnutChartLabels: string[];
  public doughnutChartData: any;
  public doughnutChartColors: any[];
  public doughnutChartOptions: any;



  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  ngOnInit() {
    //--- Bar Chart ---
    this.barChartLabels = [
      'Tramitadas',
      'Pendientes',
      'Resueltas',
    ];
    this.barChartData = [
      { data: [159, 180, 172,], label: 'Tramitación' },
    ];
    this.barChartColors = [
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.danger, 0.5),
        borderColor: this.config.colors.danger,
        hoverBackgroundColor: this.config.colors.danger
      },
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.info, 0.5),
        borderColor: this.config.colors.info,
        hoverBackgroundColor: this.config.colors.info
      },
      {
        borderWidth: 2,
        backgroundColor: this.configFn.rgba(this.config.colors.success, 0.5),
        borderColor: this.config.colors.info,
        hoverBackgroundColor: this.config.colors.info,
      }
    ];
    this.barChartOptions = {
      responsive: true,
      scales: {
        yAxes: [{
          ticks: {
            fontColor: this.configFn.rgba(this.config.colors.gray, 0.7),
            fontSize: 14,
            stepSize: 10,
            beginAtZero: true
          },
          gridLines: {
            display: true,
            zeroLineColor: this.configFn.rgba(this.config.colors.gray, 0.4),
            zeroLineWidth: 1,
            color: this.configFn.rgba(this.config.colors.gray, 0.1)
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: this.configFn.rgba(this.config.colors.gray, 0.7)
          },
          gridLines: {
            display: true,
            zeroLineColor: this.configFn.rgba(this.config.colors.gray, 0.4),
            zeroLineWidth: 1,
            color: this.configFn.rgba(this.config.colors.gray, 0.1)
          }
        }]
      },
      legend: {
        labels: {
          fontColor: this.configFn.rgba(this.config.colors.gray, 0.9),
        }
      },
      tooltips: {
        enabled: true,
        backgroundColor: this.configFn.rgba(this.config.colors.main, 0.6)
      }
    }

    let colorsArr = [];

    for (let i = 0; i < 16; i++) {
      colorsArr.push(this.getRandomColor());
    }

    //--- Doughnut/Pie Chart ---
    this.doughnutChartLabels = [
      'Incidencia 1',
      'Incidencia 2',
      'Incidencia 3',
      'Incidencia 4',
      'Incidencia 5',
      'Incidencia 6',
      'Incidencia 7',
      'Incidencia 8',
      'Incidencia 9',
      'Incidencia 10',
      'Incidencia 11',
      'Incidencia 12',
      'Incidencia 13',
      'Incidencia 14',
      'Incidencia 15',
      'Incidencia 16',
    ];
    this.doughnutChartData = [350, 420, 130, 11, 999, 500, 250, 750, 125, 875, 100, 500, 300, 200, 444, 111];
    this.doughnutChartColors = [
      {
        backgroundColor: colorsArr,
        borderColor: this.config.colors.default,
        borderWidth: 1,
        hoverBorderWidth: 2,
      }
    ];
    this.doughnutChartOptions = {
      legend: {
        labels: {
          fontColor: this.configFn.rgba(this.config.colors.gray, 0.9),
        }
      },
      tooltips: {
        enabled: true,
        backgroundColor: this.configFn.rgba(this.config.colors.main, 0.7)
      }
    }
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }


  chartClicked(e: any): void {
    //console.log(e);
  }

  chartHovered(e: any): void {
    //console.log(e);
  }



}
