import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../app.config";
import { json, select, merge } from 'd3';

@Component({
  selector: 'az-screen-c6',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenC6.component.html',
  styleUrls: ['./screenC6.component.scss'],
})
export class ScreenC6Component {
  public config: any;
  public configFn: any;

  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  ngOnInit() {
    const svg = select("svg");
    let imageWidth = 75;
    let imageCellWidth = imageWidth * 1.5;

    json<any>('/assets/data/test/asambleistas.json')
      .then(inData => {

        let partiesCounts = {};
        // transformar data a un array que contenga como clave el nombre del partido
        // y como valor un objecto con el numero de asambleistas y el color del partido
        inData.forEach(asambleista => {
          // si la clave aun no existe inicializar el objeto con el numero inicial y color
          if (!partiesCounts.hasOwnProperty(asambleista.partido)) {
            partiesCounts[asambleista.partido] = {
              numAsambl: 1,
              color: this.getRandomColor(),
            };
          } else {
            // si la clave ya existe sumar el numero de asambleistas del partido
            partiesCounts[asambleista.partido].numAsambl++;
          }
        });

        let partiesArr = Object.entries(partiesCounts);
        // console.log('partiesCount', partiesArr);

        const radius = 60;
        const totalSeats = inData.length;

        if (inData.length == 0) return "No data to display";

        const numberOfRings = this.findN(totalSeats, radius);
        const a0 = this.findA(totalSeats, numberOfRings, radius);  	// calculate seat distance

        let points = [];

        // calculate ring radii
        let rings = []
        for (let i = 1; i <= numberOfRings; i++) {
          rings[i] = radius - (i - 1) * a0
        }

        // calculate seats per ring
        let rings2 = this.distribute(rings, totalSeats);
        // let rings2 = 10;

        // let resultsList = [];
        let r, a, point;

        // build seats
        // loop rings
        let ring;
        for (let j = 1; j <= numberOfRings; j++) {
          ring = []
          // calculate ring-specific radius
          r = radius - (j - 1) * a0
          // calculate ring-specific distance
          a = (Math.PI * r) / ((rings2[j] - 1) || 1)

          // loop points
          for (let k = 0; k <= rings2[j] - 1; k++) {
            point = this.getCoordinates(r, k * a)
            point[2] = 0.4 * a0
            ring.push(point)
          }
          points.push(ring)
        }

        // fill seats
        let ringProgress = Array(points.length).fill(0);
        for (let party in partiesArr) {
          let key = partiesArr[party][0];
          console.log('key', key);
          for (let l = 0; l < parseInt(partiesArr[party][1]['numAsambl']); l++) {
            ring = this.nextRing(points, ringProgress)
            points[ring][ringProgress[ring]][3] = partiesArr[party][1]['color'];
            points[ring][ringProgress[ring]][4] = key; // the Parliamentary group
            ringProgress[ring]++
          }
        }

        // console.log('points1', points);
        points = this.merge(points);
        // console.log('points2', points);

        inData.forEach(element => {
          // console.log('element.nombre', element.nombre);
        });
        // console.log(inData);
        // add code to store the MPs' name, QID URL and gender
        for (let i = 0; i < inData.length; i++) {
          for (let j = 0; j < points.length; j++) {
            // if (inData[i].partido == points[j][4] && points[j].length == 5) {
              // console.log(points[j][4]);
            if (inData[i].partido == points[j][4]) {
              points[j][6] = inData[i].nombre;
              points[j][7] = 'https://2013-2017.observatoriolegislativo.ec/' + inData[i].urlDetalle;
              points[j][8] = inData[i].genero;
              // points[j][8] = i; // store the index of the data
              points[j][5] = i; // store the index of the data
              break;
            }
          }
        }

        a = points[0][2] / 0.1;
        // let a = points[0][2] / 0.1;

        svg.attr("viewBox", [-radius - a / 2, -radius - a / 2, 2 * radius + a, radius + a].join(','));

        // console.log(points);
        let anchors = svg.selectAll("a")
          .data(points)
          .join("a")
               .attr("href", d => d[7])
          // .attr("href", d => {console.log(d); return ''})
          // .attr("href", d => data[d[5]].mp.value)
          .attr("target", "_blank");

        let count = 0;

        anchors.append("circle")
          .attr("cx", d => d[0])
          // .attr("c", d => {console.log(d); return ''})
          .attr("cy", d => d[1])
          .attr("r", d => d[2])
          .attr("fill", d => d[3])
        // .style("opacity", function (d) {
        //   if (applyFilters(d)) {
        //     ++count;
        //     return 1;
        //   }
        //   return 0.1;
        // })

      });

  }

  // applyFilters(d) {
  //   let filters = [ filterByGender, filterByParty, filterByAge]
  //   for (let f = 0; f < filters.length; f++) {
  //     if ( filters[f](d)) continue
  //     else return false;
  //   }
  //   return true
  // }

  nextRing(rings, ringProgress) {
    let progressQuota, tQuota;
    for (let i in rings) {
      tQuota = parseFloat(((ringProgress[i] || 0) / rings[i].length) + '').toFixed(10);
      if (!progressQuota || tQuota < progressQuota) progressQuota = tQuota
    }
    for (let j in rings) {
      tQuota = parseFloat(((ringProgress[j] || 0) / rings[j].length) + '').toFixed(10);
      if (tQuota == progressQuota) return j
    }
  }

  merge(arrays) {
    let result = []
    for (let list of arrays) result = result.concat(list)
    return result
  }

  getCoordinates(r, b) {
    let x = parseFloat(r * Math.cos(b / r - Math.PI) + '').toFixed(10);
    let y = parseFloat(r * Math.sin(b / r - Math.PI) + '').toFixed(10);
    return [x, y];
  }

  distribute(votes, seats) {
    // initial settings for divisor finding
    let voteSum = 0;
    for (let party in votes) {
      voteSum += votes[party]
    }
    let low = voteSum / (seats - 2)
    let high = voteSum / (seats + 2)
    let divisor = voteSum / seats

    let parliament = this.calculateSeats(votes, divisor)

    // find divisor
    while (parliament.seats != seats) {
      if (parliament.seats < seats) low = divisor;
      if (parliament.seats > seats) high = divisor;
      divisor = (low + high) / 2;
      parliament = this.calculateSeats(votes, divisor);
    }

    return parliament.distribution;
  }

  calculateSeats(votes, divisor) {
    let distribution = {}
    let seats = 0
    for (let party in votes) {
      distribution[party] = Math.round(votes[party] / divisor)
      seats += distribution[party]
    }
    return { distribution, seats }
  }

  findN(m, r) {
    let n = Math.floor(Math.log(m) / Math.log(2)) || 1;
    let distance = this.getScore(m, n, r);

    let direction = 0;
    if (this.getScore(m, n + 1, r) < distance) direction = 1;
    if (this.getScore(m, n - 1, r) < distance && n > 1) direction = -1;

    while (this.getScore(m, n + direction, r) < distance && n > 0) {
      distance = this.getScore(m, n + direction, r);
      n += direction;
    }
    return n;
  }

  findA(m, n, r) {
    let x = (Math.PI * n * r) / (m - n);
    let y = 1 + (Math.PI * (n - 1) * n / 2) / (m - n);

    let a = x / y;
    return a;
  }

  getScore(m, n, r) {
    return Math.abs(this.findA(m, n, r) * n / r - (5 / 7))
  }

  getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  chartClicked(e: any): void {
    //console.log(e);
  }

  chartHovered(e: any): void {
    //console.log(e);
  }

}
