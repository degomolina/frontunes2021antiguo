import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import 'chart.js/dist/Chart.js';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenC6Component } from './screenC6.component';

export const routes = [
  { path: '', component: ScreenC6Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenC6Component,
  ]
})

export class ScreenC6Module { }
