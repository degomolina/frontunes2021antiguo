import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from '../../../../../app.config';
import { select, pie, arc, scaleOrdinal, svg, color } from 'd3';

@Component({
  selector: 'az-semiluna',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './semiluna.component.html',
  styleUrls: ['./semiluna.component.scss'],
})
export class SemilunaComponent {
  config: any;
  configFn: any;

  color = scaleOrdinal()
    .range(['#2b5eac', '#0dadd3', '#ffea61',]);


  data = [
    {
      label: 'JRV Pendientes',
      value: 200
    },
    {
      label: 'JRV Error en Actas',
      value: 10
    },
    {
      label: 'JRV Procesadas',
      value: 45
    },
  ];

  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  ngOnInit() {

    const width = 400;
    const height = 300; //this is the double because are showing just the half of the pie
    const radius = Math.min(width, height) / 2;
    const labelr = radius + 30; // radius for label anchor

    let vis = select('#semilunaC1')
      .append('svg') //create the SVG element inside the <body>
      .data(this.data) //associate our data with the document
      // .attr('width', width) //set the width and height of our visualization (these will be attributes of the <svg> tag
      // .attr('height', height)
      .attr("viewBox", "0 0 " + width * 1.1  + " " + height)
      .attr('preserveAspectRatio','xMinYMin')
      .append('svg:g') //make a group to hold our pie chart
      .attr('transform', 'translate(' + (width / 2) + ',' + (height / 1.5) + ')') //move the center of the pie chart from 0, 0 to radius, radius
      ;

    let arcGenerator = arc() //this will create <path> elements for us using arc data
      .innerRadius(79)
      .outerRadius(radius - 10); // full height semi pie

    // console.log('arcGenerator', arcGenerator());

    let pieGenerator = pie() //this will create arc data for us given a list of values
      .startAngle(-90 * (Math.PI / 180))
      .endAngle(90 * (Math.PI / 180))
      .padAngle(.02) // some space between slices
      .sort(null) //No! we don't want to order it by size
      .value(function (d: any) {
        return d.value;
      }); //we must tell it out to access the value of each element in our data array

    // console.log('pieGenerator', pieGenerator(this.data))
    let arcs = vis.selectAll('g.slice') //this selects all <g> elements with class slice (there aren't any yet)
      .data(pieGenerator(<any>this.data)) //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
      .enter() //this will create <g> elements for every 'extra' data element that should be associated with a selection. The result is creating a <g> for every object in the data array
      .append('svg:g') //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
      .attr('class', 'slice'); //allow us to style things in the slices (like text)

    arcs.append('svg:path')
      .attr('fill', (d, i: any) => {
        return this.color(i) + '';
      }) //set the color for each slice to be chosen from the color function defined above
      .attr('d', <any>arcGenerator); //this creates the actual SVG path using the associated data (pie) with the arc drawing function

    const textEl = arcs.append("svg:text")
      .attr("class", "labels") //add a label to each slice
      .attr("fill", "grey")
      .attr("transform", function (d:any) {
        var c = arcGenerator.centroid(d),
          xp = c[0],
          yp = c[1],
          // pythagorean theorem for hypotenuse
          hp = Math.sqrt(xp * xp + yp * yp);
        return "translate(" + (xp / hp * labelr) + ',' +
          (yp / hp * labelr) + ")";
      })
      .attr("text-anchor", "middle"); //center the text on it's origin

    textEl.append('tspan')
      .text((d, i) => {
        return this.data[i].label;
      });

    textEl.append('tspan')
      .text((d, i) => {
        return this.data[i].value;
      })
      .attr('x', '0')
      .attr('dy', '1.2em');
  }

  public chartClicked(e: any): void {
    //console.log(e);
  }

  public chartHovered(e: any): void {
    //console.log(e);
  }



}
