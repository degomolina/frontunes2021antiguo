import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import 'chart.js/dist/Chart.js';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenB3Component } from './screenB3.component';

export const routes = [
  { path: '', component: ScreenB3Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenB3Component,
  ]
})

export class ScreenB3Module { }
