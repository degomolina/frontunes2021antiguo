import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {
  geoMercator,
  geoPath,
  json,
  select,
  geoEquirectangular,
  zoom,
  scaleQuantile,
  scaleQuantize,
  range,
  selectAll,
  format,
  svg,
} from 'd3';
import { AppConfig } from "../../../../../app.config";
import { DataMapService } from './datamap.service';
import { feature } from 'topojson';
import { GlobalConstants } from '../../../../../common/global-constants';
import d3Tip from 'd3-tip';

@Component({
  selector: 'az-mapa-b2',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './datamap.component.html',
  styleUrls: ['./datamap.component.scss'],
  providers: [DataMapService]
})
export class MapaComponent implements OnInit {

  config: any;
  configFn: any;
  data: any;
  bubbles: any;
  selectedRegionName: any;
  provinces: any;
  cantones: any;
  parroquias: any;
  selectedProvince: any;

  private svg;
  private g;
  private width = 960;
  private height = 500;
  private projection;
  private pathGenerator;
  private tip;

  constructor(private _dataMapService: DataMapService, private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
    this.data = _dataMapService.getData();
    this.bubbles = _dataMapService.getBubbles();
  }

  ngOnInit() {
    this._dataMapService.getProvinces()
      .subscribe(data => {
        this.provinces = data;
        this.provinces.unshift({ id: 0, idpais: 52, provincia: 'Ecuador' })
      });
  }

  public ngAfterViewInit(): void {
    this.createSvg();

    json<any>(GlobalConstants.provincesJson)
      .then(data => {
        const geojson = feature(data, data.objects.provincias);
        this.createProjection(3850, -84, -1.5);
        this.drawProvincesMap(geojson);
      });
  }

  private createProjection(scale, centerX, centerY) {
    this.projection = geoEquirectangular()
      .scale(scale)
      .center([centerX, centerY])
      .translate([this.width / 4, this.height / 2]);
  }

  private createSvg(): void {
    let rootDiv = select('#electionMapB2');
    this.svg = rootDiv
      .append('svg:svg')
      .attr('viewBox', '0 0 ' + this.width + ' ' + this.height)
      // .attr('perserveAspectRatio', 'xMinYMid')
      .append("g");
  }

  private drawCantonesMap(data: any): void {
    this.pathGenerator = geoPath().projection(this.projection);

    function selected() {
      select('.selected').classed('selected', false);
      select(this).classed('selected', true);
    }

    this.svg.selectAll('path').data(data.features)
      .enter().append('path')
      .attr('id', (d: any) => 'prov_' + d.properties.id_prov)
      .attr('class', 'provincia')
      .attr('stroke', '#aaa')
      .attr('stroke-width', '0.5')
      .on('click', selected)
      .attr('d', this.pathGenerator);
  }

  private drawProvincesMap(data: any): void {

    let tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(d => {
        // console.log('d', d.properties.provincia);
        // console.log('this.provinces', this.provinces);
        let hoveredProvincia = this.provinces.find(prov => prov.id === d.properties.provincia);
        // console.log('hoveredProvincia', hoveredProvincia)
        return `<strong>Provincia: </strong><span class='details'>${hoveredProvincia.provincia}<br></span><strong>Population: </strong><span class='details'>'info'</span>`
      })
    // .html(d => `<strong>Country: </strong><span class='details'>${d.properties.provincia}<br></span><strong>Population: </strong><span class='details'>${format(d.properties.provincia)}</span>`);

    this.pathGenerator = geoPath().projection(this.projection);

    function selected() {
      select('.selected').classed('selected', false);
      select(this).classed('selected', true);
    }

    this.svg.call(tip);

    this.svg.selectAll('path').data(data.features)
      .enter().append('path')
      .attr('id', (d: any) => 'prov_' + d.properties.id_prov)
      .attr('transform', (d: any) => {
        if (d.properties.provincia === 9) {
          return 'translate(500, 0)';
        }
        return '';
      })
      .attr('class', 'provincia')
      .attr('stroke', '#aaa')
      .attr('stroke-width', '0.5')
      .on('click', selected)
      .on('mouseover', function (e, d) {
        tip.show(d, this);
        // select(this)
        //   .style('opacity', 1)
        //   .style('stroke-width', 3);
      })
      .on('mouseout', function (e, d) {
        tip.hide(d, this);
        // select(this)
        //   .style('opacity', 0.8)
        //   .style('stroke-width', 0.3);
      })
      .attr('d', this.pathGenerator);
  }

  private drawParroquiasMap(data: any): void {
    this.pathGenerator = geoPath().projection(this.projection);

    function selected() {
      select('.selected').classed('selected', false);
      select(this).classed('selected', true);
    }
    // console.log('data', data)
    this.svg.selectAll('path').data(data.features)
      .enter().append('path')
      // .attr('id', (d: any) => 'prov_' + d.properties.id_prov)
      .attr('class', 'parroquia')
      .attr('stroke', '#aaa')
      .attr('stroke-width', '0.5')
      .on('click', selected)
      .attr('d', this.pathGenerator);
  }

  private emptySvg() {
    selectAll("#electionMapB2 svg").remove();
  }

  private loadCantonesSelect(idProvince) {
    this._dataMapService.getCantonesOfProvince(idProvince)
      .subscribe(data => {
        this.cantones = data;
      });
  }

  private loadParroquiasSelect(cantonId) {
    this._dataMapService.getParroquiasOfCanton(cantonId)
      .subscribe(data => {
        // console.log('data_parro', data)
        this.parroquias = data;
      });
  }

  onCantonesSelectChange(cantonId) {
    this.loadParroquiasSelect(cantonId);

    json<any>(GlobalConstants.parroquiasJson)
      .then(data => {
        // console.log('data', data);
        const geojson = feature(data, data.objects.ecuador_parroquias);
        // console.log(geojson);

        // geojson.features = geojson.features
        //   .filter((d) => {
        //     console.log('hols', d.properties.DPA_CANTON, cantonId)
        //     return d.properties.DPA_CANTON == 171;
        //   });

        this.emptySvg();
        this.createSvg();

        this.projection = geoEquirectangular()
          .fitSize([this.width, this.height], geojson);
        this.drawParroquiasMap(geojson);
      });
  }

  onProvinceSelectChange(province) {
    this.cantones = [];
    this.parroquias = [];
    this.loadCantonesSelect(province);
    if (province == '0') {
      this.emptySvg();
      this.createSvg();
      json<any>(GlobalConstants.provincesJson)
        .then(data => {
          const geojson = feature(data, data.objects.provincias);
          this.createProjection(3850, -84, -1.5);
          this.drawProvincesMap(geojson);
        });
      return;
    }

    json<any>(GlobalConstants.cantonesJson)
      .then(data => {
        const geojson = feature(data, data.objects.cantones);
        // console.log(geojson);

        geojson.features = geojson.features
          .filter((d) => {
            return d.properties.provincia == province;
          });

        this.emptySvg();
        this.createSvg();

        this.projection = geoEquirectangular()
          .fitSize([this.width, this.height], geojson)
        this.drawCantonesMap(geojson);
      });
  }

}
