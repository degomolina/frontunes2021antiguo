import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../app.config";


@Component({
  selector: 'az-screen-b5',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenB5.component.html'
})
export class ScreenB5Component {
    public config:any;
    public configFn:any;

    constructor(private _appConfig:AppConfig){
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
    }

    ngOnInit() {
    }

    public chartClicked(e:any):void {
    //console.log(e);
    }

    public chartHovered(e:any):void {
    //console.log(e);
    }



}
