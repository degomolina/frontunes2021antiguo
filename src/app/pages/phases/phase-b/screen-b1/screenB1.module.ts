import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenB1Component } from './screenB1.component';

export const routes = [
  { path: '', component: ScreenB1Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenB1Component,
  ]
})

export class ScreenB1Module { }
