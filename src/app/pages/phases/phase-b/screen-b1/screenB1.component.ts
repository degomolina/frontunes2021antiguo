import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../../../app.config";


@Component({
  selector: 'az-screen-a4',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './screenB1.component.html'
})
export class ScreenB1Component  { 
    public config:any;
    public configFn:any;

    constructor(private _appConfig:AppConfig){
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;       
    } 

    ngOnInit() { 
    }
}
