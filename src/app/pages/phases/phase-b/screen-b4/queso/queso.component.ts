import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from '../../../../../app.config';
import { select, pie, arc, scaleOrdinal, svg, interpolate, } from 'd3';


@Component({
  selector: 'az-queso',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './queso.component.html'
})
export class QuesoComponent {
  public config: any;
  public configFn: any;

  color = scaleOrdinal()
    .range(['#2b5eac', '#0dadd3', '#ffea61',]);

  data = [
    {
      label: 'Votos Pendientes',
      value: 800,
    },
    {
      label: 'Votos a Candidatos',
      value: 500,
    },
    {
      label: 'Blancos',
      value: 20,
    },
    {
      label: 'Votos Nulos',
      value: 10,
    },
    {
      label: 'Abstinencias',
      value: 300,
    },
  ];

  constructor(private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
  }

  ngOnInit() {
    const width = 700;
    const height = 700; //this is the double because are showing just the half of the pie
    const radius = Math.min(width, height) / 2;
    // const labelr = radius + 30; // radius for label anchor
    let vis = select('#queso-comportamiento2')
      .append('svg') //create the SVG element inside the <body>
      .data(this.data) //associate our data with the document
      // .attr('viewBox', '0 0 ' + width + ' ' + height)
      .attr("viewBox", "0 0 " + width * 1.1  + " " + height)

      .attr('preserveAspectRatio', 'xMinYMin')
      .append('svg:g') //make a group to hold our pie chart
      .attr('transform', 'translate(' + ((width / 2) + 10) + ',' + (height / 2) + ')') //move the center of the pie chart from 0, 0 to radius, radius
      ;

    vis.append('svg:g')
      .attr('class', 'slices');
    vis.append('svg:g')
      .attr('class', 'labels');
    vis.append('svg:g')
      .attr('class', 'lines');

    let pieGenerator = pie() //this will create arc data for us given a list of values
      .padAngle(.02) // some space between slices
      .sort(null) //No! we don't want to order it by size
      .value(function (d: any) {
        return d.value;
      }); //we must tell it out to access the value of each element in our data array

    let arcGenerator = arc() // ??
      .outerRadius(radius * 0.8)
      .innerRadius(radius * 0.0); // ??

    let outerArcGenerator = arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.9);

    // vis.attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    let key = (d) => { return d.data.label; }

    /* ------- PIE SLICES -------*/
    // let slice = vis.select('.slices').selectAll('path.slice')
    //   .data(pieGenerator(<any>this.data), key);

    let arcs = vis.select('.slices').selectAll('path.slice') //this selects all <g> elements with class slice (there aren't any yet)
      .data(pieGenerator(<any>this.data)) //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
      .enter() //this will create <g> elements for every 'extra' data element that should be associated with a selection. The result is creating a <g> for every object in the data array
      .append('svg:g') //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
      .attr('class', 'slice'); //allow us to style things in the slices (like text)

    // slice.enter()
    //   .insert('path')
    //   .style('fill', (d, i: any) => this.color(i) + '')
    //   .attr('class', 'slice');

    // console.log('slice()', slice);

    arcs.append('svg:path')
      .attr('fill', (d, i: any) => {
        // console.log(d);
        return this.color(i) + '';
      }) //set the color for each slice to be chosen from the color function defined above
      .attr('d', <any>arcGenerator); //this creates the actual SVG path using the associated data (pie) with the arc drawing function


    // pendiente animacion
    // slice
    //   .transition()
    //   .duration(1000)
    //   // .attrTween('d', '16')
    //   .attrTween('d', (d: any, idx, nodeList) => {
    //   console.log('alkdfjlfkd');

    //     const i = interpolate(this.data[idx].value, d);
    //     return (t) => {
    //       this.data[idx].value =  i(t);
    //       return this.data[idx].value.toString();
    //     };
    //   });
    arcs.exit()
      .remove();

    /* ------- TEXT LABELS -------*/

    let text = select('.labels').selectAll('text')
      .data(pieGenerator(<any>this.data));

    text.enter()
      .append('text')
      .attr('dy', '.35em')
      .text(function (d: any) {
        return d.data.label;
      });



    // text.transition().duration(1000)
    //   .attrTween('transform', function (d) {
    //     this._current = this._current || d;
    //      interpolate = d3.interpolate(this._current, d);
    //     this._current = interpolate(0);
    //     return function (t) {
    //        d2 = interpolate(t);
    //        pos = outerArc.centroid(d2);
    //       pos[0] = radius * (this.midAngle(d2) < Math.PI ? 1 : -1);
    //       return 'translate(' + pos + ')';
    //     };
    //   })
    //   .styleTween('text-anchor', function (d) {
    //     this._current = this._current || d;
    //      interpolate = d3.interpolate(this._current, d);
    //     this._current = interpolate(0);
    //     return function (t) {
    //        d2 = interpolate(t);
    //       return this.midAngle(d2) < Math.PI ? 'start' : 'end';
    //     };
    //   });

    text.exit()
      .remove();

    /* ------- SLICE TO TEXT POLYLINES -------*/
    let polyline = select('.lines').selectAll('polyline')
      .data(pieGenerator(<any>this.data), key);

    polyline.enter()
      .append('polyline');

    // polyline.transition().duration(1000)
    //   .attrTween('points', function (d) {
    //     this._current = this._current || d;
    //     let interpolate = d3.interpolate(this._current, d);
    //     this._current = interpolate(0);
    //     return function (t) {
    //       let d2 = interpolate(t);
    //       let pos = outerArc.centroid(d2);
    //       pos[0] = radius * 0.95 * (this.midAngle(d2) < Math.PI ? 1 : -1);
    //       return [arc.centroid(d2), outerArc.centroid(d2), pos];
    //     };
    //   });

    polyline.exit()
      .remove();

    // let arcs = vis.selectAll('g.slice') //this selects all <g> elements with class slice (there aren't any yet)
    //   .data(pieGenerator(<any>this.data)) //associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
    //   .enter() //this will create <g> elements for every 'extra' data element that should be associated with a selection. The result is creating a <g> for every object in the data array
    //   .append('svg:g') //create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
    //   .attr('class', 'slice'); //allow us to style things in the slices (like text)

    // arcs.append('svg:path')
    //   .attr('fill', (d, i: any) => {
    //     return this.color(i) + '';
    //   }) //set the color for each slice to be chosen from the color function defined above
    //   .attr('d', <any>arcGenerator); //this creates the actual SVG path using the associated data (pie) with the arc drawing function

    const textEl = arcs.append('svg:text')
      .attr('class', 'labels') //add a label to each slice
      .attr('fill', 'grey')
      .attr('transform', function (d: any) {
        let c = arcGenerator.centroid(d),
          xp = c[0],
          yp = c[1],
          // pythagorean theorem for hypotenuse
          hp = Math.sqrt(xp * xp + yp * yp);
        return 'translate(' + (xp / hp * radius) + ',' +
          (yp / hp * radius) + ')';
      })
      .attr('text-anchor', 'middle'); //center the text on it's origin

    textEl.append('tspan')
      .text((d, i) => {
        return this.data[i].label;
      });

    textEl.append('tspan')
      .text((d, i) => {
        return this.data[i].value;
      })
      .attr('x', '0')
      .attr('dy', '1.2em');
  }

  midAngle(d) {
    return d.startAngle + (d.endAngle - d.startAngle) / 2;
  }



  chartClicked(e: any): void {
    //console.log(e);
  }

  chartHovered(e: any): void {
    //console.log(e);
  }

}
