import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import 'chart.js/dist/Chart.js';
import { DirectivesModule } from '../../../../theme/directives/directives.module';
import { ScreenB4Component } from './screenB4.component';
import { MapaComponent } from './mapa/datamap.component';
import { SemilunaComponent } from './semiluna/semiluna.component';
import { BarChartComponent } from './barchart/barchart.component';
import { QuesoComponent } from './queso/queso.component';

export const routes = [
  { path: '', component: ScreenB4Component, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ScreenB4Component,
    MapaComponent,
    SemilunaComponent,
    BarChartComponent,
    QuesoComponent,
  ]
})

export class ScreenB4Module { }
