import { Component, ViewEncapsulation } from '@angular/core';
import {
  geoMercator,
  geoPath,
  json,
  select,
  geoEquirectangular,
  zoom,
  scaleQuantile,
  scaleQuantize,
  range,
} from 'd3';
import { AppConfig } from "../../../app.config";
import { DataMapService } from './datamap.service';
import { feature } from 'topojson';

@Component({
  selector: 'az-datamap',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './datamap.component.html',
  styleUrls: ['./datamap.component.scss'],
  providers: [DataMapService]
})
export class DatamapComponent {

  public config: any;
  public configFn: any;
  public data: any;
  public bubbles: any;
  public selectedRegionName: any;

  constructor(private _dataMapService: DataMapService, private _appConfig: AppConfig) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
    this.data = _dataMapService.getData();
    this.bubbles = _dataMapService.getBubbles();
  }

  public ngAfterViewInit(): void {

    // const projection = geoMercator()
    //   .scale(4100)
    //   // .translate([-1200, 1400])
    //   .center([-80,-1.5]);
    const width = 960,
      height = 500;

    let svg = select('#electionMap')
      .append('svg:svg')
      .attr('width', width)
      .attr('height', height)
      .attr('viewBox', '0 0 ' + width + ' ' + height)
      .attr('perserveAspectRatio', 'xMinYMid')
      .attr('id', "sizer-map")
      .attr('class', "sizer")
      .call(zoom);

    let main = svg.append("g")
      .attr('transform', 'translate(0,0)')
      .attr('width', width)
      .attr('height', height)
      .attr('class', 'main');

    let rect = svg.append("rect")
      .attr("width", width)
      .attr("height", height)
      .attr("class", "overlay")
      .style("fill", "none")
      .style("pointer-events", "all");
    let mapContainer = svg.append("g");

    let tooltip = select("#tooltipContainer")
      .append("div")
      .attr("class", "");
    tooltip.html(" ");

    const projection = geoEquirectangular();
    projection.scale(4100);
    projection([-3.0026, 16.7666]);
    projection.center([-80, -1.5]);
    projection.translate([width / 2, height / 2]);

    // add d3 zoom behaviour to map container.
    const mapZoom = zoom()
      .scaleExtent([1, 10])
      .on("zoom", zoomed);

    const pathGenerator = geoPath().projection(projection);

    function zoomed() {
      svg
        .selectAll('path') // To prevent stroke width from scaling
        .attr('transform', 2);
    }

    function selected() {
      select('.selected').classed('selected', false);
      select(this).classed('selected', true);
    }


    json<any>('/assets/data/ecuador_parroquias.json')
      .then(data => {
        // console.log(data);
        const geojson = feature(data, data.objects.ecuador_parroquias);

        // console.log('geojson', geojson);

        svg.selectAll('path').data(geojson.features)
          .enter().append('path')
          .attr('class', 'country')
          .attr('stroke', '#aaa')
          .attr('stroke-width', '0.5')
          // .attr('fill', 'transparent')
          // .attr("name", function(_, d2) {return d2.properties.DPA_DESPAR;})
          .attr('viewBox', '0 0 ' + width + ' ' + height)
          .attr('perserveAspectRatio', 'xMinYMid')
          .attr('id', "sizer-map")
          .attr('class', "sizer")
          .on('click', selected)
          // .on("mouseover", function (d, d2) {
          //   console.log(d2.properties.DPA_DESPAR);
          //   select(this).attr("class", "region hover");
          //   select(this).on("mouseout", function (d) {
          //     select(this).attr("class", "region");
          //   })
          // })
          .on("mousemove", function(_, d2:any) {
            let label = d2.properties.DPA_DESPAR;
            tooltip.classed('hidden', false)
              .html(label)
          })
          .call(zoom)
          .attr('d', pathGenerator);

      });
  }
}
