export class GlobalConstants {
    public static apiURL: string = 'https://controlelectoral.unionporlaesperanza.com/api/';
    public static siteTitle: string = 'This is example of ItSolutionStuff.com';

    // geo/topojsons paths
    public static provincesJson = '/assets/data/geojsons/provincias_poligonos_v4.json';
    public static cantonesJson = '/assets/data/geojsons/cantones_poligonos_v4.json';
    public static parroquiasJson = '/assets/data/geojsons/nxparroquias_2015_fixed_wgs84.json';
    // public static parroquiasJson = '/assets/data/geojsons/parroquias.geojson';

}
