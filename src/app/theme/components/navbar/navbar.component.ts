import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { AppState } from '../../../app.state';
import { SidebarService } from '../sidebar/sidebar.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'az-navbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [SidebarService]
})

export class NavbarComponent implements OnInit, OnDestroy {
  public isMenuCollapsed: boolean = false;

  private authSub: Subscription;
  private previousAuthState = false;

  constructor(
    private _state: AppState,
    private _sidebarService: SidebarService,
    private authService: AuthService,
    private router: Router,
  ) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  public closeSubMenus() {
    /* when using <az-sidebar> instead of <az-menu> uncomment this line */
    // this._sidebarService.closeAllSubMenus();
  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
  }

  ngOnInit() {
    this.authSub = this.authService.userIsAuthenticated.subscribe(isAuth => {
      if (!isAuth && this.previousAuthState !== isAuth) {
        this.router.navigateByUrl('/login');
      }
      this.previousAuthState = isAuth;
    });
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

}
