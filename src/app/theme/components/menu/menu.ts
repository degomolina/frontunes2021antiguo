export const menuItems = [
  {
    title: 'Dashboard',
    routerLink: 'dashboard',
    icon: 'fa-home',
    selected: false,
    expanded: false,
    order: 0
  },
  {
    title: 'Fase A',
    routerLink: 'faseA',
    icon: 'fa-bar-chart',
    selected: false,
    expanded: false,
    order: 100,
    subMenu: [
      {
        title: 'Pantalla A1',
        routerLink: 'faseA/pantallaA1',
      },
      {
        title: 'Pantalla A2',
        routerLink: 'faseA/pantallaA2',
      },
      {
        title: 'Pantalla A3',
        routerLink: 'faseA/pantallaA3',
      },
      {
        title: 'Pantalla A4',
        routerLink: 'faseA/pantallaA4',
      },
      // {
      //   title: 'Pantalla A5',
      //   routerLink: 'faseA/pantallaA5',
      // },
      // {
      //   title: 'Pantalla A6',
      //   routerLink: 'faseA/pantallaA6',
      // },
    ]
  },
  {
    title: 'Fase B',
    routerLink: 'faseB',
    icon: 'fa-pie-chart',
    selected: false,
    expanded: false,
    order: 101,
    subMenu: [
      {
        title: 'Pantalla B1',
        routerLink: 'faseB/pantallaB1',
      },
      {
        title: 'Pantalla B2',
        routerLink: 'faseB/pantallaB2',
      },
      {
        title: 'Pantalla B3',
        routerLink: 'faseB/pantallaB3',
      },
      {
        title: 'Pantalla B4',
        routerLink: 'faseB/pantallaB4',
      },
      {
        title: 'Pantalla B5',
        routerLink: 'faseB/pantallaB5',
      },
      {
        title: 'Pantalla B6',
        routerLink: 'faseB/pantallaB6',
      },
      // {
      //   title: 'Pantalla B7',
      //   routerLink: 'faseB/pantallaB7',
      // },
      // {
      //   title: 'Pantalla B8',
      //   routerLink: 'faseB/pantallaB8',
      // },
    ]
  },
  {
    title: 'Fase C',
    routerLink: 'faseC',
    icon: 'fa-area-chart',
    selected: false,
    expanded: false,
    order: 102,
    subMenu: [
      {
        title: 'Pantalla C1',
        routerLink: 'faseC/pantallaC1',
      },
      {
        title: 'Pantalla C2',
        routerLink: 'faseC/pantallaC2',
      },
      {
        title: 'Pantalla C3',
        routerLink: 'faseC/pantallaC3',
      },
      {
        title: 'Pantalla C4',
        routerLink: 'faseC/pantallaC4',
      },
      {
        title: 'Pantalla C5',
        routerLink: 'faseC/pantallaC5',
      },
      {
        title: 'Pantalla C6',
        routerLink: 'faseC/pantallaC6',
      },
      // {
      //   title: 'Pantalla C7',
      //   routerLink: 'faseC/pantallaC7',
      // },
      // {
      //   title: 'Pantalla C8',
      //   routerLink: 'faseC/pantallaC8',
      // },
    ]
  },
  // {
  //   title: 'Charts',
  //   routerLink: 'charts',
  //   icon: 'fa-line-chart',
  //   selected: false,
  //   expanded: false,
  //   order: 200,
  //   subMenu: [
  //     {
  //       title: 'Ng2-Charts',
  //       routerLink: 'charts/ng2charts',
  //     },
  //   ]
  // },
  // {
  //   title: 'UI Features',
  //   routerLink: 'ui',
  //   icon: 'fa-laptop',
  //   selected: false,
  //   expanded: false,
  //   order: 300,
  //   subMenu: [
  //     {
  //       title: 'Buttons',
  //       routerLink: 'ui/buttons'
  //     },
  //     {
  //       title: 'Cards',
  //       routerLink: 'ui/cards'
  //     },
  //     {
  //       title: 'Components',
  //       routerLink: 'ui/components'
  //     },
  //     {
  //       title: 'Icons',
  //       routerLink: 'ui/icons'
  //     },
  //     {
  //       title: 'Grid',
  //       routerLink: 'ui/grid'
  //     },
  //     {
  //       title: 'List Group',
  //       routerLink: 'ui/list-group'
  //     },
  //     {
  //       title: 'Media Objects',
  //       routerLink: 'ui/media-objects'
  //     },
  //     {
  //       title: 'Tabs & Accordions',
  //       routerLink: 'ui/tabs-accordions'
  //     },
  //     {
  //       title: 'Typography',
  //       routerLink: 'ui/typography'
  //     }
  //   ]
  // },
  // {
  //   title: 'Tools',
  //   routerLink: 'tools',
  //   icon: 'fa-wrench',
  //   selected: false,
  //   expanded: false,
  //   order: 550,
  //   subMenu: [
  //     {
  //       title: 'Drag & Drop',
  //       routerLink: 'tools/drag-drop'
  //     },
  //     {
  //       title: 'Resizable',
  //       routerLink: 'tools/resizable'
  //     },
  //     {
  //       title: 'Toastr',
  //       routerLink: 'tools/toaster'
  //     }
  //   ]
  // },
  // {
  //   title: 'Mail',
  //   routerLink: 'mail/mail-list/inbox',
  //   icon: 'fa-envelope-o',
  //   selected: false,
  //   expanded: false,
  //   order: 330
  // },
  // {
  //   title: 'Calendar',
  //   routerLink: 'calendar',
  //   icon: 'fa-calendar',
  //   selected: false,
  //   expanded: false,
  //   order: 350
  // },
  // {
  //   title: 'Form Elements',
  //   routerLink: 'form-elements',
  //   icon: 'fa-pencil-square-o',
  //   selected: false,
  //   expanded: false,
  //   order: 400,
  //   subMenu: [
  //     {
  //       title: 'Form Inputs',
  //       routerLink: 'form-elements/inputs'
  //     },
  //     {
  //       title: 'Form Layouts',
  //       routerLink: 'form-elements/layouts'
  //     },
  //     {
  //       title: 'Form Validations',
  //       routerLink: 'form-elements/validations'
  //     },
  //     {
  //       title: 'Form Wizard',
  //       routerLink: 'form-elements/wizard'
  //     }
  //   ]
  // },
  // {
  //   title: 'Tables',
  //   routerLink: 'tables',
  //   icon: 'fa-table',
  //   selected: false,
  //   expanded: false,
  //   order: 500,
  //   subMenu: [
  //     {
  //       title: 'Basic Tables',
  //       routerLink: 'tables/basic-tables'
  //     },
  //     {
  //       title: 'Dynamic Tables',
  //       routerLink: 'tables/dynamic-tables'
  //     }
  //   ]
  // },
  // {
  //   title: 'Editors',
  //   routerLink: 'editors',
  //   icon: 'fa-pencil',
  //   selected: false,
  //   expanded: false,
  //   order: 550,
  //   subMenu: [
  //     {
  //       title: 'Froala Editor',
  //       routerLink: 'editors/froala-editor'
  //     },
  //     {
  //       title: 'Ckeditor',
  //       routerLink: 'editors/ckeditor'
  //     }
  //   ]
  // },
  // {
  //   title: 'Maps',
  //   routerLink: 'maps',
  //   icon: 'fa-globe',
  //   selected: false,
  //   expanded: false,
  //   order: 600,
  //   subMenu: [
  //     {
  //       title: 'Vector Maps',
  //       routerLink: 'maps/vectormaps'
  //     },
  //     {
  //       title: 'Google Maps',
  //       routerLink: 'maps/googlemaps'
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       routerLink: 'maps/leafletmaps'
  //     }
  //   ]
  // },
  // {
  //   title: 'Pages',
  //   routerLink: ' ',
  //   icon: 'fa-file-o',
  //   selected: false,
  //   expanded: false,
  //   order: 650,
  //   subMenu: [
  //     {
  //       title: 'Login',
  //       routerLink: '/login'
  //     },
  //     {
  //       title: 'Register',
  //       routerLink: '/register'
  //     },
  //     {
  //       title: 'Blank Page',
  //       routerLink: 'blank'
  //     },
  //     {
  //       title: 'Error Page',
  //       routerLink: '/pagenotfound'
  //     }
  //   ]
  // },
  // {
  //   title: 'Profile',
  //   routerLink: 'profile',
  //   icon: 'fa-file-o',
  //   selected: false,
  //   expanded: false,
  //   subMenu: [
  //     {
  //       title: 'Projects',
  //       routerLink: 'profile/projects'
  //     },
  //     {
  //       title: 'User Info',
  //       routerLink: 'profile/user-info'
  //     }
  //   ]
  // },
  // {
  //   title: 'Menu Level 1',
  //   icon: 'fa-ellipsis-h',
  //   selected: false,
  //   expanded: false,
  //   order: 700,
  //   subMenu: [
  //     {
  //       title: 'Menu Level 1.1',
  //       url: '#',
  //       disabled: true,
  //       selected: false,
  //       expanded: false
  //     },
  //     {
  //       title: 'Menu Level 1.2',
  //       url: '#',
  //       subMenu: [{
  //         title: 'Menu Level 1.2.1',
  //         url: '#',
  //         disabled: true,
  //         selected: false,
  //         expanded: false
  //       }]
  //     }
  //   ]
  // },
  // {
  //   title: 'External Link',
  //   url: 'http://themeseason.com',
  //   icon: 'fa-external-link',
  //   selected: false,
  //   expanded: false,
  //   order: 800,
  //   target: '_blank'
  // }
];
