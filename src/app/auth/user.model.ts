export class User {
  constructor(
    public cedula: string,
    private _token: string,
  ) {}

  get token() {
    return this._token;
  }

  // get tokenDuration() {
  //   if (!this.token) {
  //     return 0;
  //   }
  //   return this.tokenExpirationDate.getTime() - new Date().getTime();
  // }
}
