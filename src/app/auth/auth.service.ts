import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, from, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { User } from './user.model';

// export interface AuthResponseData {
//   kind: string;
//   idToken: string;
//   email: string;
//   refreshToken: string;
//   localId: string;
//   expiresIn: string;
//   registered?: boolean;
// }

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  private _user = new BehaviorSubject<User>(null);
  private activeLogoutTimer: any;

  get userIsAuthenticated() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return !!user.token;
        } else {
          return false;
        }
      })
    );
  }

  get userId() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.cedula;
        } else {
          return null;
        }
      })
    );
  }

  get token() {
    return this._user.asObservable().pipe(
      map(user => {
        if (user) {
          return user.token;
        } else {
          return null;
        }
      })
    );
  }

  constructor(private http: HttpClient) { }

  autoLogin() {
    return of(localStorage.getItem('authData')).pipe(
      map((storedDataTmp: any) => {
        let storedData = localStorage.getItem('authData');
        if (!storedData) {
          return null;
        }
        const parsedData = JSON.parse(storedData) as {
          cedula: string;
          token: string;
        };

        // const expirationTime = new Date(parsedData.tokenExpirationDate);
        // if (expirationTime <= new Date()) {
        //   return null;
        // }
        const user = new User(
          parsedData.cedula,
          parsedData.token,
        );
        return user;
      }),
      tap(user => {
        if (user) {
          this._user.next(user);
          // this.autoLogout(user.tokenDuration);
        }
      }),
      map(user => {
        return !!user;
      })
    );
  }

  // signup(email: string, password: string) {
  //   return this.http
  //     .post<any>(
  //       `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${
  //       environment.firebaseAPIKey
  //       }`,
  //       { email: email, password: password, returnSecureToken: true }
  //     )
  //     .pipe(tap(this.setUserData.bind(this)));
  // }

  login(cedula: string, password: string) {
    return this.http
      .post<any>(
        `https://controlelectoral.unionporlaesperanza.com/api/rpc/login`,
        { cedula: cedula, clave: password, aplicacion: 2 }
      )
      .pipe(tap(this.setUserData.bind(this, cedula)));
  }

  logout() {
    // if (this.activeLogoutTimer) {
    //   clearTimeout(this.activeLogoutTimer);
    // }
    this._user.next(null);
    localStorage.removeItem('authData');
  }

  ngOnDestroy() {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer);
    }
  }

  private autoLogout(duration: number) {
    if (this.activeLogoutTimer) {
      clearTimeout(this.activeLogoutTimer);
    }
    this.activeLogoutTimer = setTimeout(() => {
      this.logout();
    }, duration);
  }

  private setUserData(cedula: string, userData: any) {
    const user = new User(
      cedula,
      userData[0].token,
    );
    this._user.next(user);
    // this.autoLogout(user.tokenDuration);
    this.storeAuthData(
      cedula,
      userData[0].token,
    );
  }

  private storeAuthData(
    cedula: string,
    token: string,
  ) {
    const data = JSON.stringify({
      cedula: cedula,
      token: token,
    });
    localStorage.setItem('authData', data);
  }
}
