import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators} from '@angular/forms';

// import { LoadingController, AlertController } from '@ionic/angular';

import { AuthService } from './auth.service';

@Component({
  // selector: 'app-auth',
  selector: 'az-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
  isLoading = false;
  isLogin = true;
  badCredentials = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    // private loadingCtrl: LoadingController,
    // private alertCtrl: AlertController
  ) { }

  ngOnInit() { }

  authenticate(cedula: string, password: string) {
    this.isLoading = true;
    this.badCredentials = false;
    this.authService.login(cedula, password).subscribe(
      resData => {
        this.isLoading = false;
        this.router.navigateByUrl('/pages/faseA/pantallaA1');
      },
      errRes => {
        // const code = errRes.error.error.message;
        // let message = 'No se puede ingresar, intente más tarde.';
        this.badCredentials = true;
        // console.log(errRes);
        // this.showAlert(message);
      }
    );
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const cedula = form.value.cedula;
    const password = form.value.password;

    this.authenticate(cedula, password);
  }
}

export function emailValidator(control: FormControl): {[key: string]: any} {
  var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
  if (control.value && !emailRegexp.test(control.value)) {
      return {invalidEmail: true};
  }
}
